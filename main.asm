%include "colon.inc"
%include "words.inc"
section .data
word_buf: times 20 db 0xca

section .rodata
start: db "Введите ключ для поиска:", 10, 0
too_long_key: db "Введённый ключ слишком длинный", 10, 0
empty_key: db "Пустой ключ", 10, 0
no_such_key: db "Такого ключа нет в словаре(", 10, 0
val: db "А вот и ваше значение:", 10, 0

section .text
global _start

extern print_string_somewhere
extern print_newline
extern read_word
extern exit
extern find_word
extern string_length

_start:
    ;hello msg
    mov rdi, start
    mov rsi, 1
    call print_string_somewhere

    ;read key
    mov rdi, word_buf
    mov rsi, 256
    call read_word
    cmp rax, 0
    je .error_empty
    cmp rax, 256
    jl .error_long

    ;find key
    mov rdi, rax
    mov rsi, PREV_ELEMENT
    call find_word
    test rax, rax
    jz .not_found

    ;print value
    mov rsi, rax
    call string_length
    inc rax
    add rsi, rax
    push rsi
    mov rdi, val
    mov rsi, 1
    call print_string_somewhere
    pop rdi
    mov rsi, 1
    call print_string_somewhere
    call print_newline
    call exit

    ;errors
    .error_long:
        mov rdi, too_long_key
        mov rsi, 2
        call print_string_somewhere
        call exit
    .error_empty:
        mov rdi, empty_key
        mov rsi, 2
        call print_string_somewhere
        call exit
    .not_found:
        mov rdi, no_such_key
        mov rsi, 2
        call print_string_somewhere
        call exit

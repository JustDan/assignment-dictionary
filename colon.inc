%macro colon 2

%ifid %2
    %ifdef PREV_ELEMENT
        %2: dq PREV_ELEMENT
    %else
        %2: dq 0
    %endif

    %define PREV_ELEMENT %2
%else
    %fatal "Недопустимая метка"
%endif

%ifstr %1
    db   %1, 0
%else
    %fatal "Ключ должен быть строкой"
%endif
%endmacro

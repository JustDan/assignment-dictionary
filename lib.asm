global exit
global string_length
global print_string_somewhere
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_word
global read_char
global parse_uint
global parse_int
global string_copy

section .text


; Принимает код возврата и завершает текущий процесс
exit:
  mov rax, 60 ; exit syscall
  syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
xor rax, rax

.loop:
    cmp byte[rdi + rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string_somewhere:
    push rsi
    mov rsi, rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rax, 1
    syscall
    ret
; Принимает код символа и выводит его в stdout
print_char:
    push 0
    push rdi
    push rsp
    pop rdi
    mov rsi, 1
    call print_string_somewhere
    pop rdi
    pop rdi
    ret
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret
; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
  push rbx
  push rdx
  push r12
  mov r12, rsp
  mov rax, rdi
  mov rbx, 10
  dec rsp
  mov byte [rsp], 0
  .loop:
      xor rdx, rdx
      div rbx
      add rdx, '0'
      dec rsp
      mov [rsp], dl
      test rax, rax
      jz .end
      jmp .loop
  .end:
      mov rdi, rsp
      mov rsi, 1
      call print_string_somewhere
      mov rsp, r12
      pop r12
      pop rdx
      pop rbx
      ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge print_uint
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    .break:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
;rdi - указатель на 1ю строку
;rsi - указатель на 2ю строку
string_equals:
.loop:
    mov dl, byte[rsi]
    mov al, byte[rdi]
    cmp dl, al
    jne .fail
    cmp al, 0
    je .success
    inc rsi
    inc rdi
    jmp .loop
.success:
    mov rax, 1
    ret
.fail:
    mov rax, 0
    ret
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    push 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
;rdi - адрес начала буфера
;rsi - размер буфера

read_word:
    xor rbx, rbx
    xor r10, r10
    xor r11, r11

    push rbx
    push r10
    push r11

    mov r10, rdi
    mov r11, rsi

    .start:
        call read_char
        cmp al, 0x20
        je .start
        cmp al, 0x09
        je .start
        cmp al, 0x0A
        je .start
        cmp al, 0
        je .success
    .reading:
        dec r11
        cmp r11, 0
        je .fail
        mov byte[r10 + rbx], al
        inc rbx

        call read_char
        cmp al, 0x20
        je .success
        cmp al, 0x09
        je .success
        cmp al, 0x0A
        je .success
        cmp al, 0
        je .success

        jmp .reading
    .success:
        mov byte[r10 + rbx], 0
        mov rax, r10
        mov rdx, rbx
        pop r11
        pop r10
        pop rbx
        ret
    .fail:
        pop r11
        pop r10
        pop rbx
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rcx
    push rbx

    xor rcx, rcx
    xor rax, rax
    xor rdx, rdx
    mov rbx, 10

.loop:
    cmp byte[rdi + rcx], '0'
    jl .end
    cmp byte[rdi + rcx], '9'
    jg .end
    mul rbx
    mov dl, [rdi + rcx]
    sub dl, '0'
    add rax, rdx
    inc rcx
    jmp .loop

.end:
    mov rdx, rcx
    pop rbx
    pop rcx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .negative
    call parse_uint
    ret

    .negative:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
;rdi - указатель на строку
;rsi - указатель на буфер
;rdx - длина буфера
string_copy:
xor rax, rax

.loop:
    cmp rax, rdx
    je .end
    mov rcx, [rdi + rax]
    mov [rsi + rax], rcx
    inc rax
    jmp .loop

.end:
    call string_length
    mov rdi, rax
    cmp rax, rdx
    jg .fail
    mov rax, rdi
    ret

.fail:
    mov rax, 0
    ret

global find_word

extern string_equals
extern string_length

section .text

find_word:
    cmp rsi, 0
    je .error
    add rsi, 8
    push rdi
    push rsi
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 1
    jz .success
    sub rsi, 8
    mov rsi, [rsi]
    jmp find_word

.error: ;элемента неть или список пуст(
    mov rax, 0
    ret

.success: ; Достаём адрес значения
    mov rax, rsi
    ret
